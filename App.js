import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';

import LoginPage from './src/pages/LoginPage';
import HomePage from './src/pages/HomePage';
import DetailPage from './src/pages/DetailPage';

const Stack = createNativeStackNavigator();

function App() {
  return (
    <NavigationContainer>
      <Stack.Navigator screenOptions={{ headerShown: false }}>
        <Stack.Screen name="LoginPage" component={LoginPage} />
        <Stack.Screen name="HomePage" component={HomePage} options={{ headerShown: true, title: 'Perfil' }} />
        <Stack.Screen name="DetailPage" component={DetailPage} options={{ headerShown: true, title: 'Seguidores' }} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}

export default App;
