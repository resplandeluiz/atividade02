import React, { useState } from 'react'
import { StyleSheet, View, SafeAreaView, Text, Button, Image, KeyboardAvoidingView } from 'react-native'
import TextInputComponent from '../components/TextInput'
import { useNavigation } from '@react-navigation/native'
import Space from '../components/Space'

const LoginPage = () => {

  const [user, setUser] = useState('')
  const [error, setError] = useState(false)
  const navigation = useNavigation()

  const doLogin = () => {
    if (user == '') {
      setError(true)
    } else {
      setError(false)
      navigation.navigate('HomePage', { user })
    }
  }

  return (
    <SafeAreaView style={s.container}>
      <KeyboardAvoidingView
        behavior={Platform.OS === 'ios' ? 'padding' : 'height'}
        style={s.container}>

        <View style={s.container}>

          <View style={[s.container, s.firstBox]}>
            <Image source={require('../../assets/logo-git.png')} style={s.img} />
          </View>

          <View style={[s.container, s.secondBox]}>
            <Text style={s.text}>Insira um nome de usuário do GITHUB válido</Text>
            <Space spaceUnit={3} />
            <TextInputComponent placeholder='Usuário do GIT' value={user} onChange={setUser} />
            <Space spaceUnit={3} />
            {error && <Text style={s.textError}>Preencha os campos acima</Text>}
            <Button title='ENTRAR' onPress={doLogin} color='white' />
          </View>


        </View>
      </KeyboardAvoidingView>

    </SafeAreaView>
  )
}

const s = StyleSheet.create({
  container: {
    flex: 1,
  },
  firstBox: {
    alignContent: 'center',
    justifyContent: 'center',
    alignItems: 'center'
  },
  textFirstBox: {
    fontSize: 20,
  },
  secondBox: {
    paddingHorizontal: 20,
    backgroundColor: '#14191d',
    borderTopLeftRadius: 50,
    justifyContent: 'center',
    alignItems: 'center'
  },
  img: {
    width: 150,
    height: 150
  },
  textError: {
    color: 'red'
  },
  text: {
    color: 'white'
  }
})

export default LoginPage