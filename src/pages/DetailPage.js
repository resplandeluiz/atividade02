import React, { useEffect, useState } from 'react'
import { SafeAreaView, StyleSheet, View, Text, TouchableOpacity, Image, FlatList, Linking } from 'react-native'
import { useNavigation, useRoute } from '@react-navigation/native'
import api from '../api/github'
import Loading from '../components/Loading'
import Space from '../components/Space'



const Item = ({ item, onPress }) => {
  return <TouchableOpacity style={s.item} onPress={() => onPress(item)}>
    <View style={{ flexDirection: 'row' }}>
      <Image source={{ uri: item?.avatar_url }} style={s.imgList} resizeMode='contain' />
      <View style={s.boxTextItem}>
        <Text style={s.title}>{item?.login}</Text>
      </View>
    </View>
  </TouchableOpacity >
};



const DetailPage = () => {

  const navigation = useNavigation()

  const { url } = useRoute()?.params
  const [loading, setLoading] = useState(false)
  const [followers, setFollowers] = useState(null)
  const [error, setError] = useState(false)

  const getFollowers = async () => {
    setLoading(true)
    const follwers = await api.getData(null, url)
    if (!follwers?.error) {
      setFollowers(follwers)
    } else {
      setErrorRepo(true)
    }
    setLoading(false)
  }

  useEffect(() => {
    getFollowers()
  }, [])

  const openProfile = async (loginName) => {
    navigation.navigate('HomePage', { user: loginName })
  }

  return <SafeAreaView style={s.container}>
    {loading && <Loading />}


    {!loading && (
      <View style={s.container}>
        <Space />
        {error && <Text style={[s.text, s.secondText]}>Não foi possível carregar os seguidores</Text>}


        {(followers && !error) && (
          <FlatList
            data={followers}
            renderItem={({ item }) => <Item item={item} onPress={(item) => openProfile(item?.login)} />}
            keyExtractor={item => item.id}
          />
        )}
      </View>
    )}
  </SafeAreaView>
}


const s = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#14191d',
    paddingHorizontal: 20,
    paddingTop: 20
  },
  text: {
    color: 'white'
  },
  item: {
    backgroundColor: '#fff',
    padding: 20,
    marginVertical: 8,
    marginHorizontal: 16,
    borderRadius: 5
  },
  itemDescription: {
    fontSize: 12,
    color: 'gray'
  },
  imgList: {
    width: 50,
    height: 50,
    marginRight: 10,
    borderRadius: 50
  },
  boxTextItem: {
    justifyContent: 'center'
  }
})
export default DetailPage