import React, { useEffect, useState } from 'react'
import { View, StyleSheet, SafeAreaView, Text, Image, FlatList, TouchableOpacity } from 'react-native'
import api from '../api/github'
import Loading from '../components/Loading'
import Space from '../components/Space'
import { useNavigation, useRoute } from '@react-navigation/native'

const Item = ({ item, onPress }) => {
  const resolveImage = (language) => {
    switch (language) {
      case 'JavaScript':
        return require('../../assets/logo-js.png');
      case 'PHP':
        return require('../../assets/logo-php.jpeg');
      case 'TypeScript':
        return require('../../assets/logo-ts.png');
      case 'HTML':
        return require('../../assets/logo-html.png');
      default:
        return require('../../assets/no-language.jpeg');
    }
  }

  return <TouchableOpacity style={s.item} onPress={() => onPress(item)}>
    <View style={{ flexDirection: 'row' }}>
      <Image source={resolveImage(item?.language)} style={s.imgList} resizeMode='contain' />
      <View style={s.boxTextItem}>
        <Text style={s.title}>{item?.name}</Text>
        {item?.language && <Text style={[s.title, s.itemDescription]}>{item?.language}</Text>}
        {!item?.language && <Text style={[s.title, s.itemDescription]}>Linguagem não informada</Text>}
      </View>
    </View>
  </TouchableOpacity >
};

const HomePage = () => {

  const navigation = useNavigation()
  const route = useRoute()
  const { user } = route?.params

  const [loading, setLoading] = useState(false)

  const [gitData, setGitData] = useState(null)
  const [repoData, setRepoData] = useState(null)

  const [errorProfile, setErrorProfile] = useState(false)
  const [errorRepo, setErrorRepo] = useState(false)

  const loadProfileData = async () => {
    setLoading(true)

    const res = await api.getData(`/users/${user.toLowerCase()}`)
    if (!res?.error) {
      setGitData(res)
    } else {
      setErrorProfile(true)
    }

    const repo = await api.getData(`/users/${user.toLowerCase()}/repos`)
    if (!repo?.error) {
      setRepoData(repo)
    } else {
      setErrorRepo(true)
    }
    setLoading(false)
  }


  useEffect(() => {
    loadProfileData()
  }, [route?.params])


  const onPressItem = (item) => {
    console.log(item)
    navigation.navigate('DetailPage', { url: item?.owner?.followers_url })
  }


  return <SafeAreaView style={s.container}>
    {loading && <Loading />}

    {!loading && (
      <View style={s.container}>
        {!errorProfile && (
          <View style={[s.container, s.firstBox]}>
            <Space />
            <Image source={{ uri: gitData?.avatar_url }} style={s.img} />
            <Space spaceUnit={3} />
            <Text style={s.text}>{gitData?.name} - {gitData?.bio}</Text>
            <Space />
            <Text style={[s.text, s.secondText]}>{gitData?.location}</Text>
          </View>
        )}

        {errorProfile && (
          <View style={[s.container, s.firstBox]}>
            <Text style={[s.text, s.secondText]}>Não foi possível carregar o perfil</Text>
          </View>
        )}

        <View style={[s.container]}>
          <Text style={[s.text, s.titleFlatList]}>Repositórios</Text>
          <Space />

          {(repoData && !errorRepo) && (
            <FlatList
              data={repoData}
              renderItem={({ item }) => <Item item={item} onPress={onPressItem} />}
              keyExtractor={item => item.id}
            />
          )}

          {errorRepo && (
            <View style={[s.container, s.firstBox]}>
              <Text style={[s.text, s.secondText]}>Não foi possível carregar os repositórios</Text>
            </View>
          )}

        </View>
      </View>
    )}

  </SafeAreaView >
}

const s = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#14191d',
  },
  img: {
    width: 150,
    height: 150,
    borderRadius: 150,
    borderWidth: 1,
    borderColor: 'white'
  },
  text: {
    color: 'white'
  },
  secondText: {
    fontSize: 11
  },
  firstBox: {
    flex: 0.5,
    alignContent: 'center',
    justifyContent: 'center',
    alignItems: 'center',
    paddingTop: 20,
    paddingHorizontal: 20
  },
  item: {
    backgroundColor: '#fff',
    padding: 20,
    marginVertical: 8,
    marginHorizontal: 16,
    borderRadius: 5
  },
  titleFlatList: {
    marginHorizontal: 20
  },
  itemDescription: {
    fontSize: 12,
    color: 'gray'
  },
  imgList: {
    width: 50,
    height: 50,
    marginRight: 10
  },
  boxTextItem: {
    justifyContent: 'center'
  }
})

export default HomePage