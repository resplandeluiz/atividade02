const DEFAULT_URL = 'https://api.github.com'

const DEFAULT_HEADER = new Headers({
  'Authorization': 'Bearer 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',
  'Content-Type': 'application/json'
})

const getData = async (url, forceUrl = undefined) => {
  const finalUrl = forceUrl ? forceUrl : DEFAULT_URL + url
  const res = await fetch(finalUrl, { headers: DEFAULT_HEADER })
  if (res.status == 200) {
    const json = await res.json()
    return json
  }
  return {
    error: true
  }
}

export default {
  getData
}

