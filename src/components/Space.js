import React from 'react'
import { View, StyleSheet } from 'react-native'

const Space = ({ spaceUnit = 1 }) => {

  const marginVertical = 5 * spaceUnit

  const s = StyleSheet.create({
    box: {
      marginVertical: marginVertical
    }
  })

  return <View style={s.box} />
}

export default Space