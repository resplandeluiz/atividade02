import React from 'react'
import { TextInput, View, StyleSheet } from 'react-native'



const TextInputComponent = ({ onChange, value, secureTextEntry = false, placeholder = '' }) => {
  return (
    <View style={s.box}>
      <TextInput onChangeText={onChange} value={value} secureTextEntry={secureTextEntry} placeholder={placeholder} />
    </View>
  )
}

const s = StyleSheet.create({
  box: {
    width: '100%',
    padding: 10,
    borderWidth: 1,
    borderRadius: 5,
    backgroundColor: 'white'
  }
})

export default TextInputComponent