import React from 'react'

import { View, StyleSheet, ActivityIndicator } from 'react-native'

const Loading = () => {
  return <View style={s.container}>
    <ActivityIndicator size={'large'}/>
  </View>
}

const s = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  }
})

export default Loading